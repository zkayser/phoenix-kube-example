defmodule PhxKube.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    children = [
      # Start the Ecto repository
      PhxKube.Repo,
      # Start the Telemetry supervisor
      PhxKubeWeb.Telemetry,
      # Start the PubSub system
      {Phoenix.PubSub, name: PhxKube.PubSub},
      # Start the Endpoint (http/https)
      PhxKubeWeb.Endpoint
      # Start a worker by calling: PhxKube.Worker.start_link(arg)
      # {PhxKube.Worker, arg}
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: PhxKube.Supervisor]
    # Run migrations
    PhxKube.Release.migrate()
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  def config_change(changed, _new, removed) do
    PhxKubeWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
