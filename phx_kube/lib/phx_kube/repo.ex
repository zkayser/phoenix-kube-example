defmodule PhxKube.Repo do
  use Ecto.Repo,
    otp_app: :phx_kube,
    adapter: Ecto.Adapters.Postgres
end
