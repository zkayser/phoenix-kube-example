defmodule PhxKube.Timeline.Post do
  use Ecto.Schema
  import Ecto.Changeset

  schema "posts" do
    field(:body, :string)
    field(:from_node, :string)

    timestamps()
  end

  @doc false
  def changeset(post, attrs) do
    post
    |> change(%{from_node: "#{Node.self()}"})
    |> cast(attrs, [:body])
    |> validate_required([:from_node, :body])
    |> validate_length(:body, min: 2, max: 250)
  end
end
