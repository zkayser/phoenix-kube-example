defmodule PhxKube.Repo.Migrations.CreatePosts do
  use Ecto.Migration

  def change do
    create table(:posts) do
      add :from_node, :string
      add :body, :string

      timestamps()
    end

  end
end
