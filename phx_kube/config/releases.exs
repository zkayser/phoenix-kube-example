import Config

# Database configuration as a string in the following format: ecto://username:password@host:port/database_name
# Make sure you have postgres running on port 5432 and you have the default postgres user with password postgres
# or this won't work for you. You can swap out the postgres:postgres part to use your own database username
# and password if needed. Also, if you named your application differently, be sure to change out the database
# name as (phx_kube_dev) to `my_app_dev`
db_url = "ecto://postgres:postgres@postgres:5432/phx_kube_dev?pool_size=10"

config :phx_kube, PhxKube.Repo, url: db_url
