# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :phx_kube,
  ecto_repos: [PhxKube.Repo]

# Configures the endpoint
config :phx_kube, PhxKubeWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "RF7c9qMzmbMAgcfR8qUmVeP3Dnu7sbdGaE+D6d0WoxeiUhvhyCwY9LVWZFoI/QP/",
  render_errors: [view: PhxKubeWeb.ErrorView, accepts: ~w(html json), layout: false],
  pubsub_server: PhxKube.PubSub,
  live_view: [signing_salt: "F5gTDT6c"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
