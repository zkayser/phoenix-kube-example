# An Operator's Guide to Running Distributed Elixir in the Cloud: Mix Releases, Kubernetes, CI-CD & GitOps, Monitoring, and Diagnostics

If you're just getting started with a greenfield Elixir application and are considering options for operating your app in production, you're going to be faced with a multitude of options for deploying, operating, and monitoring your application. This guide is going to focus on one way to handle operational concerns with a clustered, distributed Elixir application. We're going to use a number of tools to create the groundwork for our infrastructure, starting with mix releases to build and configure our application, then setting up the release we have built to run in Docker. We'll set up an example application to run in distributed mode and then take the Docker image we built earlier and run multiple instances of our application in Kubernetes to create our cluster. After that, we'll take a look at deploying our distributed application to a cloud provider, and we will provide some basic tools to automate deployments of your application using Gitlab CI. Once we have our CI/CD steps automated, we'll switch gears to focus on observability and metrics for our application. We'll set up Telemetry to provide insights into what is going on in our application layer, publish metrics from Telemetry into a format that can be read by Prometheus, and then we'll reach for a tool called Grafana that will let us visualize and create dashboards for our metric data. The last thing we'll do is pull in a tool called Loki that integrates with Grafana to track and parse logs from our application. All of these tools will also be run through our Kubernetes cluster. There is _a lot_ to cover, so we'll break this entire process down into discrete steps. We'll start with a new Phoenix project and add a simple feature that leverages distributed Elixir/Erlang, and then we'll proceed following this plan: 

1. Build a release that works locally with `mix release`
2. Dockerize our release
3. Spin up a local Kubernetes cluster using [Minikube](https://kubernetes.io/docs/tasks/tools/install-minikube/)
4. Deploy our Kubernetes cluster to a managed Kubernetes service via a cloud provider (Digital Ocean, Google Cloud, AWS, Azure, IBM, etc.)
5. Setup CI/CD (we'll use Gitlab in this guide, but porting over to use Github Actions, CircleCI, or other CI/CD tools should be relatively straightforward)
6. Setup metrics using Telemetry to provide insights into our application
7. Install Prometheus in our Kubernetes cluster and configure it to read in metrics published by Telemetry
7. Pull in Grafana to query our Prometheus metric data and display dashboards
8. Pull in Loki to collect logs from the application container pods running on our Kubernetes cluster

## Part 1: Running Your Phoenix App in Distributed Mode Locally

Let's get started by setting up a new Phoenix project. 

---
### A Quick Word on Versions!

Before we dive in, let's get some bookkeeping out of the way. Throughout this guide, we are going to use the following Elixir, Erlang/OTP, and Phoenix versions:

- Elixir 1.10.3
- Erlang/OTP 22.3.3
- Phoenix 1.5.1

I have Elixir and Erlang versions installed via [asdf](https://asdf-vm.com/#/core-manage-asdf-vm). For instructions on installing Phoenix, checkout the [installation guide](https://hexdocs.pm/phoenix/installation.html) on Hexdocs.

If you are running Elixir, Erlang, and Phoenix versions more recent than the above versions listed, you will probably be fine for the most part. If you're running on older versions, a lot of what we do will still apply and work for you. Note, however, that you need to have at least Elixir version 1.9 to be able to run mix releases, which is a critical feature for the Kubernetes setup that we will layout here. If you are running a pre-1.9 Elixir version, I'd recommend using 1.9 or later for this guide or look into [Distillery](https://hexdocs.pm/distillery/home.html) for building releases without relying on Elixir's built-in releases feature. 

---
### Creating a New Phoenix Project

** If you already have an Elixir app that is setup to run in distributed mode and you want to use that, feel free to skip to the next section. **

With our versions setup and ready to go, let's open a terminal and run the following command:

```bash
$ mix phx.new phx_kube --live
```

Follow the prompts to install dependencies and create your database. As a starting point, we'll use a watered-down version of Chris McCord's [real-time Twitter clone](https://www.phoenixframework.org/blog/build-a-real-time-twitter-clone-in-15-minutes-with-live-view-and-phoenix-1-5) using LiveView. Instead of having a full Twitter-like timeline with likes and retweets, we'll just have a list of posts on the timeline with some content and a note saying which one of our Elixir nodes the post was created from.

Let's bootstrap everything we need by running the following command:

```bash
$ mix phx.gen.live Timeline Post posts from_node body
```

Follow the prompts to edit your `router.ex` file with the `live` routes for posts. We should now be able to go to `localhost:4000/posts` and see that we have the groundwork ready for building posts. If you click on the `New post` link, it will pop open a modal with a form to edit the properties of a post: `from_node`, and `body`. We want `body` to be editable, but we don't want users to be able to enter the `from_node` attribute. We'll leave that job to be automated by our code. Pop open the `form_component.html.leex` file and edit it to remove the label, input, and error tags for `:from_node`. For the `:body`, change the input to a textarea, like this:

```erb
<%= textarea f, :body %>
```

Now, let's head over to the `post.ex` schema module and add some length validations for the `body` attribute and `from_node`.

`lib/phx_kube/timeline/post.ex`
```elixir
schema "posts" do
  field :body, :string
  field :from_node, :string

  timestamps()
end

  @doc false
  def changeset(post, attrs) do
    post
    |> change(%{from_node: "#{Node.self()}"})
    |> cast(attrs, [:body])
    |> validate_required([:from_node, :body])
    |> validate_length(:body, min: 2, max: 250)
  end
```

With this setup, we are requiring the body to be between 2 to 250 characters, and we are defaulting the `from_node` value to display the name of the current node the post is being created on. If you go to `localhost:4000/post` and create a new post now, you should see that your `from_node` value will be the default of `nonode@nohost`. This will change when we start running the application in distributed mode and we give our nodes names.

Following along with the original real-time Twitter clone by Chris McCord, the next thing we'll do is add a broadcast via Phoenix PubSub for new and updated posts. This will make it apparent when we are running in distributed mode that our nodes are connected and able to communicate with each other in real-time. Let's open up `timeline.ex` and edit the `create_post/1` and `update_post/2` functions:

`lib/phx_kube/timeline.ex`
```elixir
  def create_post(attrs \\ %{}) do
    %Post{}
    |> Post.changeset(attrs)
    |> Repo.insert()
    |> broadcast(:post_created)
  end
  
  def update_post(%Post{} = post, attrs) do
    post
    |> Post.changeset(attrs)
    |> Repo.update()
    |> broadcast(:post_updated)
  end

  def delete_post(%Post{} = post) do
    Repo.delete(post)
    |> broadcast(:post_deleted)
  end
```

Add the `broadcast/2` private function and a `subscribe/0` function to allow our LiveView to subscribe to the PubSub events we're broadcasting here:

```elixir
  def subscribe do
    Phoenix.PubSub.subscribe(PhxKube.PubSub, "posts")
  end
  
  defp broadcast({:error, _} = error, _), do: error
  defp broadcast({:ok, post}, event) do
    Phoenix.PubSub.broadcast(PhxKube.PubSub, "posts", {event, post})
    {:ok, post}
  end
```

The next thing we'll want to do is move over to the LiveView module for the index view at `lib/phx_kube_web/live/post_live/index.ex` and use the `Timeline.subscribe/0` function we just wrote above so we can update the UI whenever any user creates, updates, or deletes a post.

```elixir
  @impl true
  def mount(_params, _session, socket) do
    if connected?(socket) do
      Timeline.subscribe()
    end
    {:ok, assign(socket, :posts, fetch_posts())}
  end
```

We'll only call `Timeline.subscribe/0` if the socket is connected since `mount/3` on the LiveView will be invoked twice: once to render the html markup from the server on the initial request, and the second time after the html markup has been sent down to the client and the client connects back to the LiveView process via a websocket connection. The `connected?/1` function ensures that a websocket connection has been established between server and client, and we can use that here to prevent a duplicate subscription to the Timeline pubsub topic. 

Now, to respond to these events, let's place the following code below the handle_event/3 definition:

```elixir
  @valid_events [:post_created, :post_updated, :post_deleted]

  @impl true
  def handle_info({event, _post}, socket) when event in @valid_events do
    {:noreply, assign(socket, :posts, fetch_posts())}
  end
```

This will listen to events being broadcast over our PubSub connection, and we will refresh the list of posts with all of the posts that happen to be in the database at the time this `handle_info` callback function gets run. This will even happen instantly across the nodes in our distributed cluster! You can test this out by setting up a local cluster on your machine by having two instance of our server running on separate ports. 

---
### Elixir Clusters on Your Development Machine and Connecting Nodes Locally

The simplest way to get a feel for running an Elixir application in distributed mode is to run two nodes on your development machine and connect them together. To do that with a basic Phoenix application like we have here, let's start by changing some of the configuration in the `config/dev.exs` file. 

First, let's define a `port` variable that we'll set to the value of an environment valuable called `PORT` if present, or otherwise default to the standard port of 4000. In `dev.exs`, define port like this:

```elixir
port = System.get_env("PORT", "4000") |> String.to_integer()
```

Now, in the config for `PhxKubeWeb.Endpoint`, let's change the line: 
```elixir
http: [port: 4000],
``` 

and replace it with our port variable:
```elixir
http: [port: port],
```

Now we can start two instances of our server and run them on separate ports. To do that, open up two terminal tabs. In one run this command:

```bash
$ iex --name one@127.0.0.1 -S mix phx.server
```

and in the other run:

```bash
$ PORT=4001 iex --name two@127.0.0.1 -S mix phx.server
```

You can now open up both instances in a browser, one at the default `localhost:4000` and the other at `localhost:4001`.

There is one step that we're missing to get these two nodes connected with each other, though. Since we opened both in an iex session, we can call Elixir's `Node.connect/1` function to connect the two nodes together. In the first terminal window for the node we named "one@127.0.0.1", run this command to connect to the other node:

```elixir
Node.connect(:"two@127.0.0.1")
```

Now, if you run `Node.list/0`, you should see a list with one element -- the name of the node you just connected to. The connection works in both directions, too, so if you open your iex session for the node we named "two@127.0.0.1" and run `Node.list/0`, you should get back a list with `[:"one@127.0.0.1"]`.

If you pull up the `/posts` index route in your browser with one tab open for each of our server instances, you should see that creating a post in one instance updates the post list in both of your open browser tabs, and you should see the name of the node you created the post from appear in the "From Node" column of our post list.

## Part 2: Building a Release

With the initial project setup out of the way in Part 1 of this series and the ability to run multiple instances in distributed mode on our local machine using mix phx.server to run the servers, let's turn our attention towards building an Elixir release.

### What is a Release, Anyway?

In Elixir and Erlang-land, a release is a way to precompile your application code and package it up into a single, discrete unit that combines your code along with the Erlang runtime your application code relies on. This means that you get a single artifact out of a release that you can place and run on your production servers without those production servers even needing to have Elixir or Erlang installed (since the runtime is packaged together with your release). For Phoenix apps, this has several benefits over running your application with `mix`:

1. You don't need to install Elixir and its dependencies directly on to your production server.
2. Releases give you a lightweight footprint because they do not require your source code to be included in production artifacts, and in addition, the Erlang and Elixir standard libraries are stripped down to include only the parts that you actually use.
3. Releases take advantage of code preloading so all modules are loaded in the Erlang VM as soon as your application boots up; when you run your application using `mix phx.server`, the VM will run in what is called "interactive" mode and will dynamically load modules when they are used for the first time -- doing this in production may cause a spike in response times when the application boots because there could potentially be a significant number of other modules the VM needs to load
4. You get more control over configuration and customization, with the ability to hook into compile-time configuration as well as runtime/boot-time configuration; you also get full control over the VM flags used to start the system
5. Releases come with scripts to help you manage your application including scripts that let you start, stop, and restart your running application, connect into the running system remotely, execute RPC calls, and more.

### Running your Application as a Release Locally

Now that we are armed with that information, let's look at how to get started running our application as a release. There are just a couple of steps we have to take to set up our app to run a release locally, but keep in mind as we do this, releases are best suited for deployment rather than local development. The goal here is to provide you with an idea of how releases are structured and how they run. In most cases, you would be using releases to run applications on your production, staging, and CI servers. When running locally for development purposes, use whatever strategy works best for you and your team, whether that means running your development server with `mix phx.server` or using a tool like Docker Compose. 

Let's take a quick pass at building a release using our development config and see what happens when we run it. From the root of our application, run this command:

```bash
$ MIX_ENV=dev mix release
```

You should see output that looks something like this:

```
Release created at _build/dev/rel/phx_kube!

    # To start your system
    _build/dev/rel/phx_kube/bin/phx_kube start

Once the release is running:

    # To connect to it remotely
    _build/dev/rel/phx_kube/bin/phx_kube remote

    # To stop it gracefully (you may also send SIGINT/SIGTERM)
    _build/dev/rel/phx_kube/bin/phx_kube stop

To list all commands:

    _build/dev/rel/phx_kube/bin/phx_kube
```

Cool! Looks like building a release was not too complicated after all :).
Let's try a couple of different things with the release we've built to highlight some aspects of how releases work. First of all, let's open a couple of terminal tabs. In the first one, start the release we just built.

```bash
$ _build/dev/rel/phx_kube/bin/phx_kube start
```

You might notice that running the start command right now doesn't print out any of the normal output to standard out like we see when we start our app with `mix phx.server`. We'll get back to why this is happening, but for now, move over to your other terminal tab. Here, we'll connect to our release in a remote iex session and run a couple of commands to prove that our release is really running and we can interact with it. To start up a remote iex session, run

```bash
$ _build/dev/rel/phx_kube/bin/phx_kube remote
```

You should be dropped into an iex session. Now that we're here, let's interact with our application.

```elixir
iex(phx_kube@Zacherys-MBP)1> alias PhxKube.Repo
PhxKube.Repo
iex(phx_kube@Zacherys-MBP)2> alias PhxKube.Timeline.Post
PhxKube.Timeline.Post
iex(phx_kube@Zacherys-MBP)3> Repo.all(Post)
[
  %PhxKube.Timeline.Post{
    __meta__: #Ecto.Schema.Metadata<:loaded, "posts">,
    body: "some conetnt",
    from_node: "two@127.0.0.1",
    id: 15,
    inserted_at: ~N[2020-05-11 20:49:23],
    updated_at: ~N[2020-05-11 20:49:23]
  },
  %PhxKube.Timeline.Post{
    __meta__: #Ecto.Schema.Metadata<:loaded, "posts">,
    body: "some stuff",
    from_node: "two@127.0.0.1",
    id: 18,
    inserted_at: ~N[2020-05-11 20:57:36],
    updated_at: ~N[2020-05-11 20:57:36]
  },
  %PhxKube.Timeline.Post{
    __meta__: #Ecto.Schema.Metadata<:loaded, "posts">,
    body: "tag this",
    from_node: "two@127.0.0.1",
    id: 13,
    inserted_at: ~N[2020-05-11 20:44:40],
    updated_at: ~N[2020-05-11 21:00:24]
  }
]
```

Great! We can see that we are connected to our application and we can connect to the repo and fetch data from our database. That is all well and good, but you might notice that if you visit `http://localhost:4000` as usual, it looks like our server isn't running. We can address this in `dev.exs` and fix the "hang" that appears to happen when we started our release earlier. We'll make some minor changes to our config in `dev.exs` that we'll want to revert after we prove that we can run our release in development and connect to it as usual from a browser. 

In `config/dev.exs`, modify the `config :phx_kube, PhxKubeWeb.Endpoint` to look like this:

```elixir
config :phx_kube, PhxKubeWeb.Endpoint,
  http: [port: port],
  debug_errors: true,
  server: true,
  code_reloader: false,
  check_origin: false
```

Next, let's stop the release we started earlier and build another release to read in the updates to our config.

```bash
$ MIX_ENV=dev mix release

...

# start the new release
$ _build/dev/rel/phx_kube/bin/phx_kube start
```

You should see the standard Phoenix output now. 

```bash
[info] Running PhxKubeWeb.Endpoint with cowboy 2.7.0 at 0.0.0.0:4000 (http)
[info] Access PhxKubeWeb.Endpoint at http://localhost:4000
```

You can verify that the server is running and you can reach the application from a browser as usual by going to `http://localhost:4000`.

### Mixing in Runtime Configuration with `config/releases.ex`


Now we have a release up and running on our local machines that pulls all of its configuration directly from our `config/dev.exs` file. This configuration is read in at _compile_ time, so by the time our release is finished building and before we even run our code, our configuration is set in stone. That works just fine for this minimal set up that we have right now, especially since we are running our code on the same machine that we are building (compiling) on. 

But what if we compiled our code on a separate machine from the machine we are intending to run on (as is often the case for production applications, such as when you build on a CI server or build machine of some sort, then ship the executable artifact that you create on the build machine to your production servers)? Some problems you are bound to run into are issues with configuration and sensitive information such as database credentials. If, for example, you store sensitive information such as database credentials on your production servers as environment variables, how do you tell your build machines to compile your code with that information? You _could_ store the same exact environment variables on your build machines, but that may not work for everyone, and it would add some extra overhead to keep your build machines in sync with your production machines. The work you have to do will only get more complex as your system grows larger. 

Mix releases provide an escape hatch to handle exactly this scenario. Instead of defining _all_ of your configuration in the environment-specific files under `./config`, you can create a `config/releases.exs` config file that will overwrite your compile time configuration and be read in when your application boots -- this means, for example, if I have a `DATABASE_URL` environment variable on my production servers, I can read from that environment variable to configure my application to connect to a production database _without_ having to store that same sensitive information on my build servers. 

Let's experiment locally with what runtime configuration looks like just to get a feel for it. To demonstrate, let's first go into `config/dev.exs` and remove our database configuration. Delete or comment out this code in `dev.exs`:

```elixir
# Configure your database
config :phx_kube, PhxKube.Repo,
  username: "postgres",
  password: "postgres",
  database: "phx_kube_dev",
  hostname: "localhost",
  show_sensitive_data_on_connection_error: true,
  pool_size: 10
```

With that code gone, let's first build another release as we have up to this point to see what goes wrong if you try to start your application with no database configuration.

```bash
$ mix release
```

Try to start the release:

```bash
$ _build/dev/rel/phx_kube/bin/phx_kube start
```

You should see the application crash immediately. The error should be something like `(RuntimeError) connect raised KeyError exception: key :database not found.` This shows clearly that we are unable to connect to our database. Let's keep the database configuration commented out in `dev.exs` for now and fix this issue using runtime configuration instead. 

To get started, let's create a `releases.exs` file under the `config` directory. Fill it out with this code:

```elixir
import Config

# Database configuration as a string in the following format: ecto://username:password@host:port/database_name
# Make sure you have postgres running on port 5432 and you have the default postgres user with password postgres
# or this won't work for you. You can swap out the postgres:postgres part to use your own database username
# and password if needed. Also, if you named your application differently, be sure to change out the database
# name (phx_kube_dev) to `my_app_dev`
db_url = "ecto://postgres:postgres@localhost:5432/phx_kube_dev"

config :phx_kube, PhxKube.Repo, url: db_url
```

Let's build another release and see if this gets us past the crash we saw after removing our database config from `dev.exs`.

```bash
$ mix release
```

And run:

```bash
_build/dev/rel/phx_kube/bin/phx_kube start
```

And it looks like we're good this time! You can take a look at `http://localhost:4000/posts` to see that we are indeed connected to the database -- we're able to read in existing posts and create new ones. And that's all there is to setting up runtime configuration! 

### A Quick Word on Security and Best Practices

In the example `releases.exs` we created above, we did something that I would highly recommend against for production code: we wrote out the credentials for our database connection in plain text. This file will almost certainly be checked in to your git repository, and it is best to avoid exposing any sensitive information like this in plain text. 

To get around this, you'll most likely want to store this information in environment variables that exist on your production servers and read your configuration in from those. Typically, in `releases.exs`, that would be done like this:

```elixir
db_url = System.fetch_env!("DATABASE_URL")
```

What is nice about this is that, if the `DATABASE_URL` environment variable is not set, `System.fetch_env!/1` will raise an exception so that you will know immediately if something went wrong and be able to respond accordingly right away.

### VM and Environment Configuration with Releases

If you interact with our application running locally right now, you'll notice that we lost one more relatively key piece of functionality that we got from running with `mix phx.server`: there is no apparent way to set the node name for our releases. You might recall that when we initially set up our application, we could run two separate nodes using a command like `iex --name one@127.0.0.1 -S mix phx.server`. This allowed us to configure the underlying BEAM instance by passing in a fully-qualified node name (`one@127.0.0.1`). How do we account for this in the mix release model?

The answer is to generate a couple more scripts, one of which will allow us to set the name for the nodes in our release. Mix releases comes with a command you can run to generate samples of the scripts you will need. At the command line in the root directory, run this command:

```bash
$ mix release.init
```

You should see this output:

```bash
* creating rel/vm.args.eex
* creating rel/env.sh.eex
* creating rel/env.bat.eex
```

The `mix release.init` command will create a `./rel` directory for you and populate it with three sample files. When you build a release, the `mix release` command will know to look for these files to incorporate any VM flags or environment-level configurations into the build step. Note that each of the files created are `.eex` files, so you can write executable Elixir code in these files just as you would in, for example, a `.html.eex` file. Since we're concerned with setting the node name, we'll need to adjust the `env.sh.eex` file (or, if you are on Windows, the `env.bat.eex` file). Let's pop that open and see what it looks like:

```sh
#!/bin/sh

# Sets and enables heart (recommended only in daemon mode)
# if [ "$RELEASE_COMMAND" = "daemon" ] || [ "$RELEASE_COMMAND" = "daemon_iex" ]; then
#   HEART_COMMAND="$RELEASE_ROOT/bin/$RELEASE_NAME $RELEASE_COMMAND"
#   export HEART_COMMAND
#   export ELIXIR_ERL_OPTIONS="-heart"
# fi

# Set the release to work across nodes. If using the long name format like
# the one below (my_app@127.0.0.1), you need to also uncomment the
# RELEASE_DISTRIBUTION variable below.
# export RELEASE_DISTRIBUTION=name
# export RELEASE_NODE=<%= @release.name %>@127.0.0.1
```

As you can see, the file is already populated with some commented out code that more or less points out what we need to do. Really, for running the release locally, all we need to do is uncomment the last two lines:

```sh
export RELEASE_DISTRIBUTION=name
export RELEASE_NODE=<%= @release.name %>@127.0.0.1
```

It is interesting to note that mix has provided us with a `@release` assign that we can use in the script to set our node name. Since we only have one release (one application), when we run `mix release`, mix will default the name of the release struct to the name of our application, `phx_kube`. If you have multiple applications in an umbrella app or something along those lines, you will have to explicitly set the name of the release you are trying to build and run your release using `mix release _name_of_the_release_you_want_to_build_`. Let's build another release with our configuration in place and see what effect this has on our running application.

```bash
$ mix release
# Start the release
$ _build/dev/rel/phx_kube/bin/phx_kube start
```

Now let's open a browser and go to `http://localhost:4000/posts` and create a new post to see our node name config in action. Here is what it looks like for me:   

![alt image showing node name](/screenshots/node_name_example.png)

Pretty cool! Now we have finer-grained of our application environment and configuration. 

Before we move on, let's delete `config/releases.exs` for now (we'll get back to it soon) and restore our `config/dev.exs` file to what its initial state:

`config/dev.exs`
```elixir
use Mix.Config

port =
  case System.get_env("PORT") do
    nil -> 4000
    port -> String.to_integer(port)
  end

# Configure your database
config :phx_kube, PhxKube.Repo,
  username: "postgres",
  password: "postgres",
  database: "phx_kube_dev",
  hostname: "localhost",
  show_sensitive_data_on_connection_error: true,
  pool_size: 10

# For development, we disable any cache and enable
# debugging and code reloading.
#
# The watchers configuration can be used to run external
# watchers to your application. For example, we use it
# with webpack to recompile .js and .css sources.
config :phx_kube, PhxKubeWeb.Endpoint,
  http: [port: port],
  debug_errors: true,
  code_reloader: true,
  check_origin: false,
  watchers: [
    node: [
      "node_modules/webpack/bin/webpack.js",
      "--mode",
      "development",
      "--watch-stdin",
      cd: Path.expand("../assets", __DIR__)
    ]
  ]

# ## SSL Support
#
# In order to use HTTPS in development, a self-signed
# certificate can be generated by running the following
# Mix task:
#
#     mix phx.gen.cert
#
# Note that this task requires Erlang/OTP 20 or later.
# Run `mix help phx.gen.cert` for more information.
#
# The `http:` config above can be replaced with:
#
#     https: [
#       port: 4001,
#       cipher_suite: :strong,
#       keyfile: "priv/cert/selfsigned_key.pem",
#       certfile: "priv/cert/selfsigned.pem"
#     ],
#
# If desired, both `http:` and `https:` keys can be
# configured to run both http and https servers on
# different ports.

# Watch static and templates for browser reloading.
config :phx_kube, PhxKubeWeb.Endpoint,
  live_reload: [
    patterns: [
      ~r"priv/static/.*(js|css|png|jpeg|jpg|gif|svg)$",
      ~r"priv/gettext/.*(po)$",
      ~r"lib/phx_kube_web/(live|views)/.*(ex)$",
      ~r"lib/phx_kube_web/templates/.*(eex)$"
    ]
  ]

# Do not include metadata nor timestamps in development logs
config :logger, :console, format: "[$level] $message\n"

# Set a higher stacktrace during development. Avoid configuring such
# in production as building large stacktraces may be expensive.
config :phoenix, :stacktrace_depth, 20

# Initialize plugs at runtime for faster development compilation
config :phoenix, :plug_init_mode, :runtime
```

## Part 3: Dockerizing Our Release

In the last section, we created a release to run on our development machines with relatively standard configuration. In this part, we will start laying the groundwork to containerize our application using Docker. This will set us up well for the next section where we will cluster our application using Kubernetes. 

Before we begin, make sure that you have Docker available on your system. If you need to install Docker, you can find a Docker distribution for your operating system [here](https://docs.docker.com/get-docker/). 

### The Dockerfile

The first thing we need to begin Dockerizing our application is a `Dockerfile` at the root of our application. Go ahead and create it like this:

```bash
# cd to root directory
$ touch Dockerfile
```

We will start out by building our release in Docker using our development configuration from before to illustrate some concepts that will be useful to understand later when we start looking at production releases and deploying to a Kubernetes cluster.

Open the Dockerfile and add the following code:

```Dockerfile
# Pull in the Elixir Alpine linux image to use 
# for building our release. Feel free to use a
# different version, but keep in mind it is best
# to keep the Elixir version here consistent with
# what you and your team are using in development.
FROM elixir:1.10.3-alpine AS builder

ENV MIX_ENV=dev

# Set our working directory to /usr/local/#{app_name}
WORKDIR /usr/local/phx_kube

# This step installs all the build tools we'll need
RUN apk update \
  && apk upgrade --no-cache \
  && apk add --no-cache \
  nodejs-npm \
  alpine-sdk \
  openssl-dev \
  && mix local.rebar --force \
  && mix local.hex --force

# Copies our app source code into the build container
COPY . .

# Compile Elixir
RUN mix do deps.get, deps.compile, compile


# Compile Javascript
RUN cd assets \
  && npm rebuild node-sass \
  && npm install \
  && npm run deploy \
  && cd .. \
  && mix phx.digest


# Build Release and move to /opt/release directory
RUN mkdir -p /opt/release \
  && mix release \
  && mv _build/${MIX_ENV}/rel/phx_kube /opt/release

# Create the runtime container
FROM erlang:22.3.3-alpine as runtime

# Install runtime dependencies
RUN apk update \
  && apk upgrade --no-cache \
  && apk add --no-cache gcc

# Set working directory again, this time inside
# of the runtime container.
WORKDIR /usr/local/phx_kube

# Copy the build we created using the builder image
# into our runtime container
COPY --from=builder /opt/release/phx_kube .

# Set the default command to be run when
# the container starts -- (start the release)
CMD [ "bin/phx_kube", "start" ]

# Set up a container healthcheck
HEALTHCHECK --interval=30s --timeout=30s --start-period=5s --retries=2 \
  CMD nc -vz -w 2 localhost 4000 || exit 1
```

A few comments on the structure of the Dockerfile are in order. The first thing to notice is that we are using two different base images: one to build the release, and the other to create the runtime tools that we'll need to run the release. We're relying on the Alpine Linux images of both Elixir and Erlang, respectively, to fulfill these roles. The Alpine images are much more lightweight and make for a smaller footprint, which will help prevent unnecessary resources from being used on our production and CI servers. You'll also notice that we are still building the release using the `dev` environment which we configured in the previous step. We'll work through getting different environments set up a bit later on in this walkthrough, but not a lot will change from the Dockerfile.

The first half of the Dockerfile, from the line that begins `FROM elixir:1.10.3-alpine AS builder` to `FROM erlang:22.3.3-alpine AS runtime` is where we specify all the tools and dependencies we need in order to build our application, its assets, and our release. We use the Elixir Alpine image for this so we have access to tools like `mix` already available to us. Once we build the release in this part of the Dockerfile,we move it to the `/opt/release` directory and then move on to building out the runtime image we'll use to run our release. After bringing in one dependency (gcc) we need for the runtime, we copy the release from the builder image into the runtime image and then specify the command to run when a container is started from our output image: `bin/phx_kube start`, which will start the release. We also add a `HEALTHCHECK` for the container at the end.

One last thing we should do before building our first image, however, is to prevent a number of files from our build machine from being copied into the builder image. The Docker directive near the top of our Dockerfile that says `COPY . .` means: "Copy all the things from my current working directory and all of its children into the current working directory of the builder image." The problem with this approach is that there are quite a few files we _do not need or want_ to have added to our builder image. There are a couple of reasons for this: 1) unused files will unnecessarily bloat the size of the image we produce, and 2) some of the files we have on our build machine may pollute or corrupt the build image since our development machine and the docker image are based on different operating systems. The solution to this problem is to create a `.dockerignore` file. The `.dockerignore` file works in much the same way that a `.gitignore` file does -- you specify paths and files that you don't want to include in your Docker builds and whatever files and directories match the patterns you specify get ignored. For our purposes, let's create a `.dockerignore` with the following contents:

```text
_build/
.elixir_ls/
.git/
.vscode/
deps/
priv/static/
test/
.dockerignore
.tool-versions
.formatter.exs
.gitignore
Dockerfile
README.md
```

This will leave out things that are not needed to actually _run_ our application -- things like test files, dependencies, and other development environment artifacts that are useful for managing the code for the project like `.gitignore` and `.elixir_ls` can safely be left out of our Docker builds.

With our `.dockerignore` and `Dockerfile` set up and ready to go, let's try building our first image. Open up a terminal, go to the root directory of our `phx_kube` project (or whatever name you might have used) and run the following command:

```bash
# -t creates an identifying tag for the image being built.
# I am calling mine zkayser/phx_kube:test -- zkayser is my Docker Hub username,
# phx_kube is the name of the application, and `test` is the _tag_ itself.
# Later, we will push our images to a container registry like Docker Hub, and
# when we do, we can use a command like `docker push {username_or_organization}/{project}:{tag_name}`.
$ docker build -t zkayser/phx_kube:test .
```

Once the image finishes building, you should see some output that looks something like this:

```text
Successfully built cd8e44b6c2b9
Successfully tagged zkayser/phx_kube:test
```

We now have our very own Docker image that we can try to run! Let's see what happens when we do:

```bash
# Remember to replace zkayser/phx_kube:test with whatever your Docker image + tag is.
$ docker run -it zkayser/phx_kube:test
```

And then observe what happens:

```bash
[error] Postgrex.Protocol (#PID<0.2893.0>) failed to connect: ** (DBConnection.ConnectionError) tcp connect (localhost:5432): connection refused - :econnrefused
[error] Postgrex.Protocol (#PID<0.2895.0>) failed to connect: ** (DBConnection.ConnectionError) tcp connect (localhost:5432): connection refused - :econnrefused
# .... MORE POSTGRES CONNECTION ERRORS
```

Hmmm, not necessarily what we had in mind. The problem is that the local network _inside of our Docker container_ does not look the same as it does on our host machine, i.e. `localhost:5432` on our host machine where Postgres is running is not accessible from inside of our Docker container with the way that we have set it up. There are a few ways around this issue, but what I've found to work best for me is to use a tool called [Docker Compose](https://docs.docker.com/compose/install/). 

### Running our Application in Docker Compose

Docker Compose will allow us to define a set of containers, and create a Docker network for us that will allow the containers to communicate between each other without us having to manually create a Docker network (you can learn about Docker networks [here](https://docs.docker.com/network/)to learn how to connect to your host machine's Postgres instance from a Docker container). 

Before we get started, let's make sure that we have Docker Compose installed:

```bash
$ which docker-compose
# Should return the path the docker-compose binary if you have it installed on your machine.
```

If you need to install Docker Compose, download the version for your operating system [here](https://docs.docker.com/compose/install/).

With Docker Compose, we will be defining _multiple_ images that are needed to run our system. To begin with, we'll use two images -- 1) our release image that we defined and built above, and 2) the official Postgres image. You define system "services" for Docker Compose inside of a `docker-compose.yml` file. Let's create one at the root of our directory and fill it out with the following contents:

```yml
version: '3'
services:
  web:
    build:
      context: ./
    ports:
      - "4000:4000"
    volumes:
      - .:/code
    depends_on:
      - postgres
  postgres:
    image: "postgres:12.3"
    environment:
      POSTGRES_USER: postgres
      POSTGRES_PASSWORD: postgres
      PGDATA: /var/lib/postgresql/data
    volumes:
      - ./tmp/db:/var/lib/postgresql/data
```

This declares a `web` service and a `postgres` service, where the `web` service is our application and will be built using the Dockerfile we defined above based on `build.context: ./` entry in the yaml file. We also declare that our `web` service `depends_on` the `postgres` service, which means that the `postgres` service must be running for the `web` service to run. 

We also make use of a `volumes` here, which is a way for us to persist data from our containers on our host machine so that we don't lose data when we destroy a container or create a new one. The syntax we are using for defining volumes here follows the pattern `{host_machine_path}:{container_path}`, which essentially defines a spot on the host machine that can hold on to the data at `{container_path}` in the Docker container. For more information on volumes, check out the documentation [here](https://docs.docker.com/compose/compose-file/#volumes).

One last note about the `docker-compose` file above is that we are defining hardcoded environment variables so the Postgres image can start up with a default user and know where to store our data inside of the container. 

Now that we have a minimal `docker-compose.yml`, let's build and run our services and see if we can run our system.

```bash
# From the root directory where `docker-compose.yml` is defined:
$ docker-compose up
```

After the images build and run, you'll see the same error we saw earlier about not being able to connect to Postgres. This is because we need to change the `db_url` variable we defined in `releases.exs` earlier so that it uses our Docker network to reach the Postgres container rather than trying to connect on localhost. 

Remember that in the `releases.exs` file, we defined our `db_url` variable to be `"ecto://postgres:postgres@localhost:5432/phx_kube_dev"`. We need to change `localhost` to point to the Postgres container running on our Docker network. When we run `docker-compose`, it will create a network location for each of the services we defined in `docker-compose.yml`. Since we called our Postgres service `postgres`, we can refer to the Postgres container as `postgres` from our other services. What this means for us is that we can change our `db_url` in `releases.exs` to `"ecto://postgres:postgres@postgres:5432/phx_kube_dev"` instead and we should be able to get around the connection error.

Let's give it a shot. First run `docker-compose down` to stop any running services, then run `docker-compose build` to rebuild our application image and make sure that the new value for `db_url` gets reflected in the release we are going to run:

```bash
$ docker-compose down
$ docker-compose build
$ docker-compose up
```

When the application spins up, you'll see that we are now running into another error related to our Postgres connection: `[error] Postgrex.Protocol (#PID<0.2886.0>) failed to connect: ** (Postgrex.Error) FATAL 28000 (invalid_authorization_specification) no pg_hba.conf entry for host "172.23.0.3", user "postgres", database "phx_kube_dev", SSL off`.

This is happening because we have not created the `phx_kube_dev` database inside of our Postgres container. When running locally using `mix phx.server`, you usually create the database you need by running `mix ecto.create`, then run any migrations you might have by running `mix ecto.migrate`. We'll address migrations in a bit, but for now what we need to do is add a little more configuration to our Postgres image. 

In the `docker-compose.yml` file, we can add an extra environment variable to the Postgres configuration to give a different name to the default database the image starts up with. We can do this using the `POSTGRES_DB` environment variable. Let's open go back to `docker-compose.yml` and add this variable to the Postgres configuration:

```yaml
postgres:
    image: "postgres:12.3"
    environment:
      POSTGRES_USER: postgres
      POSTGRES_PASSWORD: postgres
      POSTGRES_DB: phx_kube_dev
      PGDATA: /var/lib/postgresql/data
    volumes:
      - ./tmp/db:/var/lib/postgresql/data
```

Alright, let's clear sping down the docker-compose system, and restart:

```bash
$ docker-compose down && docker-compose build && docker-compose up
```

You may run into another Postgres connection error again here. If you're seeing a `DBConnection.ConnectionError` or other Postgrex error when you run `docker-compose up`, there is one more piece of configuration we should take care of in our Elixir code inside of `releases.exs`, and we should also remove the `./tmp/db` directory where we are mounting the database files from the container to make sure we are starting from a clean slate.

Let's address our Elixir code first. Right now, we have our database url defined as:

```elixir
db_url = "ecto://postgres:postgres@postgres:5432/phx_kube_dev"
```

We can, and _should_, add more fine-grained configuration to the database inside of the url. The connection string can also take query parameters to configure things like connection pool size, which is exactly what we are going to do here. Change your `db_url` variable to the following:

```elixir
db_url = "ecto://postgres:postgres@postgres:5432/phx_kube_dev?pool_size=10"
```

This will give us a connection pool with up to 10 connections. The second thing we will want to do before running the system again will be to remove the `tmp` directory where we are mounting the Postgres container's database files onto our host filesystem. You can delete the directory in your UI if you wish, or just run `rm -rf ./tmp` from the root directory of the project. 

And one last thing before we start up again -- let's clear out any dangling Docker images, containers, and networks we've stored on our host machine:

```bash
$ docker image prune
$ docker container prune
$ docker network prune
```


This will ensure we're free from lingering artifacts hanging around from previous builds. Let's run one more time:

```bash
$ docker-compose build && docker-compose up
```

If you get a `tcp connection error` when you run this time, it most likely means that Postgres did not start up and make itself available for connections before your Phoenix application started. Press `ctrl-c` to stop docker-compose and try running again if you run into this issue (the Postgres image will have some extra work to do since we deleted our volume, which can slow its start time on the first run). 

Once the container starts up and we get past our issues connecting to Postgres, you should be able to go to `http://localhost:4000` and see that the application has loaded. However, if you visit `http://localhost:4000/posts`, you'll run into one last issue we have with the database: we never ran our migrations against the Dockerized database, so we see an error indicating that: `ERROR 42P01 (undefined_table) relation "posts" does not exist`. There are a couple of ways to get around this when running your applications via releases. The recommended approach is to create a Release module that exposes `migrate` and `rollback` functions that can be used as one-off commands on your release using the script provided by releases: `bin/my_release eval #{some_command}`. Let's create the `Release` module first.

```elixir
defmodule PhxKube.Release do
  @moduledoc """
  Functions for running database migrations
  and rollbacks from a release.
  """

  @app :phx_kube

  def migrate do
    load_app()

    for repo <- repos() do
      {:ok, _, _} = Ecto.Migrator.with_repo(repo, &Ecto.Migrator.run(&1, :up, all: true))
    end
  end

  def rollback(repo, version) do
    load_app()
    {:ok, _, _} = Ecto.Migrator.with_repo(repo, &Ecto.Migrator.run(&1, :down, to: version))
  end

  defp repos do
    Application.fetch_env!(@app, :ecto_repos)
  end

  defp load_app do
    Application.load(@app)
  end
end
```

Looking through the code in our `Release` module, you will see that we are using the `Ecto.Migrator` module directly to run migrations and rollbacks. We also have to make sure our app is loaded (in our case, `:phx_kube` is the name of our otp app) to fetch all of our ecto repos from the application environment (we will only have one repo here). 

Now that we have the `Release` module in place, we could open up a terminal and run, open a shell in our application container, and run `./bin/phx_kube eval PhxKube.Release.migrate()` to run our migrations, but we will first have to spin down our docker-compose environment and rebuild the application. 

One other approach would be to automatically run the `PhxKube.Release.migrate/0` function when our application starts up by calling it from `Application.start/2`. Let's try this approach for now. Open up `application.ex` and edit the `start/2` function as follows:

```elixir
defmodule PhxKube.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    children = [
      # Start the Ecto repository
      PhxKube.Repo,
      # Start the Telemetry supervisor
      PhxKubeWeb.Telemetry,
      # Start the PubSub system
      {Phoenix.PubSub, name: PhxKube.PubSub},
      # Start the Endpoint (http/https)
      PhxKubeWeb.Endpoint
      # Start a worker by calling: PhxKube.Worker.start_link(arg)
      # {PhxKube.Worker, arg}
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: PhxKube.Supervisor]
    # Run migrations
    PhxKube.Release.migrate()
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  def config_change(changed, _new, removed) do
    PhxKubeWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
```

Now let's spin down our docker-compose environment, rebuild, and run again to see how we're doing:

```bash
$ docker-compose down
$ docker-compose build
$ docker-compose up
```

Navigate to `http://localhost:4000/posts` and you should see the page loads up fine! You should also be able to create a new post, edit it, and delete it. 

## Part 4: Distributed Elixir Locally with Kubernetes and Minikube

In this section, we will begin working on getting a full, viable setup to run our entire system in a way that reflects a production-like environment. The central piece of our effort to coordinate our system will be Kubernetes, and we will use a tool called Minikube that let's us run a Kubernetes cluster locally inside a virtual machine.

If you are not yet familiar with Kubernetes, you may be wondering what exactly it is and how it fits in our infrastructural picture. In short, Kubernetes is an open-source, cloud-agnostic platform for managing and orchestrating "containerized workloads and services". What this means practically for us is that we can tell Kubernetes that we want to run some number of certain containers and it will take care of pulling images to build the containers, starting them, running them, and monitoring them or otherwise coordinating different parts of the container lifecycle according to configuration that we control. We will be using Docker as our core container technology, but Kubernetes provides support for other types of containers as well. 

The other key takeaway from our description of Kubernetes is that it is _cloud-agnostic_. For the most part, this means that once we have our Kubernetes configuration in place, we can run our entire system on any of the large cloud providers (AWS, Google Cloud, Azure, Digital Ocean, etc.), on our local machine via Minikube, or even on machines in our own data centers provided we have installed and configured the Kubernetes control plane administrative tools necessary. In reality, there are some minor differences between cloud providers, but by and large a Kubernetes setup that works in one place will also work in another. 

Kubernetes also provides a compelling way for us to run distributed Elixir applications: we can configure Kubernetes to automatically scale up or scale down the number of Elixir nodes (or "pods" in Kubernetes terminology) based on load and resources available. Furthermore, we can configure our Elixir application using one of a number of Hex packages to automatically discover other Elixir nodes to connect to inside the Kubernetes network. 

If you remember back to the section where we learned how to run a "cluster" of Elixir nodes on our development machine earlier, you might recall that we gave our two nodes fully-qualified names (`one@127.0.0.1` and `two@127.0.0.1`), and in order to connect them together, we had to open an iex session on one and run the command: `Node.connect(:"two@127.0.0.1")`. There are some problems with this that become obvious when you begin to think about how this would work in the cloud with a system that autoscales our Elixir nodes up and down depending on system load. First of all, it is unlikely that we would know exactly what the fully-qualified names for each node will be in such an environment if we are allowing a tool like Kubernetes to assign internal IP addresses to our nodes as they spin up. Further complicating this issue, it is not necessarily feasible to have a "system admin" or "system operator" on standby waiting for whenever a new node gets created to open a remote shell on the node and run `Node.connect/1` to connect it to the rest of the cluster. We'll use a Hex package called Libcluster to take care of discovering new nodes that get spun up and connecting them to the rest of our cluster. 

There is a mountain of material to get through in this section, so let's start small and build up to a more robust configuration. To get things rolling, let's install Minikube on our development machine. Check out the official documentation for setting up Minikube on your operating system [here](https://kubernetes.io/docs/tasks/tools/install-minikube/). If you don't already have the Kubernetes command line tool, `kubectl`, installed, you will want to take care of that now as well. The documentation for installing and setting up `kubectl` can be found [here](https://kubernetes.io/docs/tasks/tools/install-kubectl/).

### A Basic Setup With Minikube

With Minikube installed on your machine, we'll want to make sure that we start it up before running any Kubernetes-related commands. You may have to also install a hypervisor on your machine in addition to `minikube` itself -- the official documentation has more detailed information for different operating systems. To run Minikube, you can usually just run the `minikube start` command, but on the first attempt at starting up Minikube, you may have to also specify which hypervisor you want to use as your "driver". I am using a hypervisor called `hyperkit`, so I can start up Minikube as follows:

```bash
$ minikube start --driver=hyperkit
```

